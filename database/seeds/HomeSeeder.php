<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('homes')->insert(
            array(
                array(
                    'description' => 'Bonjour et bienvenue sur mon site cv. Je m\'appelle Antonio et je suis en B2 Informatique à Toulouse Ynov Campus',
                    'name' => 'RAKOTOMALALA Rantoniaina Antonio',
                    'age' => '21 ans',
                    'phone' => '07 76 62 60 76',
                    'email' => 'rantoniainaantonio.rakotomalala@ynov.com'
                ),
            )
        );
    }
}
