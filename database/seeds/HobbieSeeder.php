<?php

use Illuminate\Database\Seeder;

class HobbieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('hobbies')->insert(
            array(
                array(
                    'name' => 'musique',
                    'description' => 'guitare, piano, ukulele, tam tam'
                ),
            )
        );
    }
}
