<?php

use Illuminate\Database\Seeder;

class FormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('formations')->insert(
            array(
                array(
                    'date' => '2015 -2018',
                    'name' => 'Conservatoire National des Arts et Métiers',
                    'title' => 'Réseaux et Systèmes d\'informations',
                    'description' => '86 ECTS'
                ),
            )
        );
    }
}
