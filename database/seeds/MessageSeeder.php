<?php

use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('messages')->insert(
            array(
                array(
                    'name' => 'hihi',
                    'email' => 'z@z.com',
                    'message' => 'hello everyone'
                ),
            )
        );
    }
}
