<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ExperienceSeeder::class);
        $this->call(FormationSeeder::class);
        $this->call(HobbieSeeder::class);
        $this->call(HomeSeeder::class);
        $this->call(MessageSeeder::class);
    }
}
