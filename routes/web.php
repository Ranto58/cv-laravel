<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@normal');
Route::get('home', 'HomeController@normal');
Route::get('formations', 'FormationController@normal');
Route::get('experiences', 'ExperienceController@normal');
Route::get('hobbies', 'HobbieController@normal');
Route::get('/contact', function () {
    return new App\Mail\Contact();
});

Auth::routes();
Route::middleware('auth')->group(function () {
    
    Route::get('admin', 'AdminController@index');
    Route::get('admin/home', 'AdminController@home');
    Route::get('admin/formations', 'AdminController@formation');
    Route::get('admin/experiences', 'AdminController@experience');
    Route::get('admin/hobbies', 'AdminController@hobbie');
    Route::get('admin/messages', 'AdminController@message');

    Route::resources([
        'admin' => 'AdminController',
        'admin/home' => 'HomeController',
        'admin/formations' => 'FormationController',
        'admin/experiences' => 'ExperienceController',
        'admin/hobbies' => 'HobbieController',
        'admin/messages' => 'MessageController',
    ]);
});

Route::post('/contact', 'HomeController@PostMail');
