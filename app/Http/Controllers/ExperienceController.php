<?php

namespace App\Http\Controllers;

use App\Experience;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $experiences = DB::table('experiences')->get();
        return view('admin.experiences', ['experiences' => $experiences, 'mode' => 'edit']);
    }

    public function normal()
    {
        //
        $experiences = DB::table('experiences')->get();
        return view('experiences.index', ['experiences' => $experiences, 'mode' => 'normal']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('experiences.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $inputs = $request->except('_token');
        // $experience = new Experience();
        // foreach($inputs as $key->$value){
        //     $experience->$key = $value;
        // }
        // $experience->save();
        
        // return redirect(route('experiences.index'));
        $inputs = $request->except('_token');

        $experience = new Experience();

        $experience->date = $request->input('date');
        $experience->name = $request->input('name');
        $experience->title = $request->input('title');
        $experience->description = $request->input('description');
        $experience->save();

        return redirect('admin/experiences');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $experiences = Experience::find($id);
        return view('experiences.edit', ['experiences' => $experiences]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token','_method');
        $experience = Experience::find($id);
        foreach($inputs as $key => $value){
            $experience->$key = $value;
        }
        $experience->save();

        return redirect('admin/experiences');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $experience = Experience::find($id);
        $experience->delete();

        return redirect('admin/experiences');
        // return redirect(route('experiences.index'))->with('success', 'expérience supprimée avec succès!');
    }
}
