<?php

namespace App\Http\Controllers;

use App\Formation;
use App\Experience;
use App\Hobbie;
use App\Message;
use DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function index(){
        return view('admin.index');
    }
    public function home(){
        $home = DB::table('homes')->get();
        return view('admin.home', ['home' => $home, 'mode' => 'edit']);
    }
    public function formation(){
        $formations = DB::table('formations')->get();
        return view('admin.formations', ['formations' => $formations, 'mode' => 'edit']);
    }
    public function experience(){
        $experiences = DB::table('experiences')->get();
        return view('admin.experiences', ['experiences' => $experiences, 'mode' => 'edit']);
    }
    public function hobbie(){
        $hobbies = DB::table('hobbies')->get();
        return view('admin.hobbies', ['hobbies' => $hobbies, 'mode' => 'edit']);
    }
    public function message(){
        $messages = Message::all();
        return view('admin.messages', ['messages' => $messages, 'mode' => 'edit']);
    }
}
