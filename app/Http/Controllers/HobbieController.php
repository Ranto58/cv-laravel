<?php

namespace App\Http\Controllers;

use App\Hobbie;
use Illuminate\Http\Request;
use DB; 

class HobbieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hobbies = DB::table('hobbies')->get();
        return view('admin.hobbies', ['hobbies' => $hobbies, 'mode' => 'edit']);
    }

    public function normal()
    {   
        $hobbies = DB::table('hobbies')->get();
        return view('hobbies.index', ['hobbies' => $hobbies, 'mode' => 'normal']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('hobbies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $inputs = $request->except('_token');

        $hobbie = new Hobbie();

        $hobbie->name = $request->input('name');
        $hobbie->description = $request->input('description');
        $hobbie->save();

        return redirect('admin/hobbies');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hobbie  $hobbie
     * @return \Illuminate\Http\Response
     */
    public function show(Hobbie $hobbie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hobbie  $hobbie
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $hobbie = Hobbie::find($id);
        return view('hobbies.edit', ['hobbie' => $hobbie]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hobbie  $hobbie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $inputs = $request->except('_token', '_method');
        $hobbie = Hobbie::find($id);
        foreach($inputs as $key => $value){
            $hobbie->$key = $value;
        }
        $hobbie->save();
        return redirect('admin/hobbies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hobbie  $hobbie
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $hobbie = Hobbie::find($id);
        $hobbie->delete();
        return redirect('admin/hobbies');
    }
}
