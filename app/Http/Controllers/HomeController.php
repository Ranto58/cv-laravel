<?php

namespace App\Http\Controllers;

use App\Home;
use App\Message;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $home = DB::table('homes')->get();
        return view('admin.home', ['home' => $home, 'mode' => 'edit']);
    }
    public function normal()
    {
        $home = DB::table('homes')->get();
        return view('home.index', ['home' => $home, 'mode' => 'normal']);

    }

    public function edit($id)
    {
        $home = Home::find($id);
        return view('home.edit', ['home' => $home]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token', '_method');
        $home = Home::find($id);
        foreach ($inputs as $key => $value) {
            $home->$key = $value;
        }
        $home->save();
        return redirect('admin/home');
    }

    public function PostMail(Request $request)
    {
        $message = new Message();

    
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->message = $request->input('message');

        $message->save();
        return redirect('contact');
    }
}
