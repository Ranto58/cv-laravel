<?php

namespace App\Http\Controllers;

use App\Formation;
use Illuminate\Http\Request;
use DB;


class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $formations = DB::table('formations')->get();
        return view('admin.formations', ['formations' => $formations, 'mode' => 'edit']);
    }
    
    public function normal()
    {
        //
        $formations = DB::table('formations')->get();
        return view('formations.index', ['formations' => $formations, 'mode' => 'normal']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('formations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $inputs = $request->except('_token');
        $formation = new Formation();
        $formation->date = $request->input('date');
        $formation->name = $request->input('name');
        $formation->title = $request->input('title');
        $formation->description = $request->input('description');
        $formation->save();

        return redirect('admin/formations');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function show(Formation $formation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $formation = Formation::find($id);
        return view('formations.edit', ['formation' => $formation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $inputs = $request->except('_token', '_method');
        $formation = Formation::find($id);
        foreach($inputs as $key => $value){
            $formation->$key = $value;
        }
        $formation->save();
        return redirect('admin/formations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $formation = Formation::find($id);
        $formation->delete();

        return redirect('admin/formations');
    }
}
