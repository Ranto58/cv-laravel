

@foreach($experiences as $experience)

<div class="container mt-4">
    <div class="row  mb-2">
        <div class="col py-4 px-lg-5 border bg-light text-center">{{$experience->date}}</div>
        <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->name}}</div>
        <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->title}}
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->description}}</div>
        </div>
        @if($mode == 'edit')
        <div class="row d-flex ml-2" style="height: 40px; margin-top:55px;">
            <a class="btn btn-primary" href="{{route('experiences.edit', ['experience' => $experience->id])}}"
                role="button">
                <i class="fas fa-pen"></i>
            </a>
        </div>
        <form method="POST" action="{{route('experiences.destroy', ['experience' => $experience->id])}}">
            @method('DELETE')
            @csrf
            <div class="row  ml-4" style="height: 40px; margin-top:55px;">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </form>
        @endif
    </div>
</div>
@endforeach
@if($mode == 'edit')
<div class="row">
    <div class="mx-auto">
        <a class="btn btn-primary" href="{{route('experiences.create')}}" role="button">
            <i class="fas fa-plus-circle"></i>
        </a>
        <div>
        </div>
        @endif