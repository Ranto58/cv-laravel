@foreach($hobbies as $hobbie)
<div class="container mt-3">
    <div class="row  mb-2">
        <div class="col py-4 px-lg-5 border bg-light text-center">{{$hobbie->name}}</div>
        <div class="col py-4 px-lg-5 border bg-light text-center">{{$hobbie->description}}</div>
        @if($mode == 'edit')
        <div class="row d-flex ml-2" style="height: 40px; margin-top:25px;">
            <a class="btn btn-primary" href="{{route('hobbies.edit', ['hobby' => $hobbie->id])}}" role="button">
                <i class="fas fa-pen"></i>
            </a>
        </div>
        <form method="POST" action="{{route('hobbies.destroy', ['hobby' => $hobbie->id])}}">
            @method('DELETE') @csrf
            <div class="row  ml-4" style="height: 40px; margin-top:25px;">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </form>
        @endif
    </div>
</div>


@endforeach
@if($mode == 'edit')
<div class="row">
    <div class="mx-auto">
        <a class="btn btn-primary" href="{{route('hobbies.create')}}" role="button">
            <i class="fas fa-plus-circle"></i>
        </a>
    </div>
</div>
@endif