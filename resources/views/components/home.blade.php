@foreach($homes as $home)
<div class="card mb-3">
  <img src="/antonio.jpg" class="card-img-top" alt="antonio" style="height:300px; object-fit:cover; object-position:50% 40%;">
  <div class="card-body">
    <h5 class="card-title">Antonio - CV</h5>
    <p class="card-text">{{$home->description}}</p>
    <p class="card-text">Je me présente...</p>
  </div>
</div>
<div class="col w-50">
  <div class="col text-left border bg-light mb-3 rounded-lg" style="height: 3rem; padding-top: 0.6em;">{{$home->name}}</div>
</div>
<div class="col w-50">
  <div class="col text-left border bg-light mb-3 rounded-lg" style="height: 3rem; padding-top: 0.6em;">{{$home->age}}</div>
</div>
<div class="col w-50">
  <div class="col text-left border bg-light mb-3 rounded-lg" style="height: 3rem; padding-top: 0.6em;">{{$home->phone}}</div>
</div>
<div class="col w-50">
  <div class="col text-left border bg-light mb-3 rounded-lg" style="height: 3rem; padding-top: 0.6em;">{{$home->email}}</div>
</div>
@if($mode == 'edit')
<div class="row d-flex ml-2" style="height: 40px; margin-top:25px;">
  <a class="btn btn-primary" href="{{route('home.edit', ['home' => $home->id])}}" role="button">
    <i class="fas fa-pen"></i>
  </a>
</div>
@endif
@endforeach