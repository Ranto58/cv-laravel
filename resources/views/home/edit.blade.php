@extends('admin.template')

@section('title', 'Modifier - Home')

@section('content')
<form class="ml-3 mr-3" method="POST" action="{{route('home.update', ['home' => $home->id])}}">
    @method('PUT')
    @csrf
    <div class="form-group col-md-6">
        <label for="date">description</label>
        <input type="text" name="description" class="form-control" id="description" value="{{$home->description}}">
    </div>
    <div class="col">
        <div class="form-group col-md-6">
            <label for="date">name</label>
            <input type="text" name="name" class="form-control" id="name" value="{{$home->name}}">
        </div>
        <div class="form-group col-md-6">
            <label for="date">Age</label>
            <input type="text" name="age" class="form-control" id="age" value="{{$home->age}}">
        </div>
        <div class="form-group col-md-6">
            <label for="date">Phone</label>
            <input type="text" name="phone" class="form-control" id="phone" value="{{$home->phone}}">
        </div>
        <div class="form-group col-md-6">
            <label for="date">Email</label>
            <input type="text" name="email" class="form-control" id="email" value="{{$home->email}}">
        </div>
        <button type="submit" class="btn btn-primary" style="margin-left:50%;">Mettre à jour</button>
</form>
@endsection