@extends('admin.template')

@section('title', 'Mes hobbies')

@section('content')
@component('components.hobbies', ['hobbies', 'hobbies' => $hobbies, 'mode' => 'edit'])
@endcomponent
@endsection