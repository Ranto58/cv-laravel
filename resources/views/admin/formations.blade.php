@extends('admin.template')

@section('title', 'Mes experiences')

@section('content')
@component('components.formations', ['formations' => $formations, 'mode' => 'edit'])
@endcomponent
@endsection