@extends('admin.template')
@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4">Bienvenue sur la section administrateur de votre CV.</h1>
      <p class="lead"><p>Ici, vous pourrez ajouter, modifier ou supprimer les contenus de votre CV</p>
      </p>
    </div>
  </div>
@endsection