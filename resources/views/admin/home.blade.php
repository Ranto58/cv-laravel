@extends('admin.template')

@section('title', 'Mon Profil')

@section('content')
@component('components.home', ['homes' => $home, 'mode' => 'edit'])
@endcomponent
@endsection