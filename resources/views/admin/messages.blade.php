@extends('admin.template')

@section('title', 'Mes messages')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Messages</h1>
            </div>
        </div>

        <div class="container mt-2">
            <div class="row">
                <div class="col py-4 px-lg-4 border bg-primary text-center w-50">Nom</div>
                <div class="col py-4 px-lg-4 border bg-primary text-center">Email</div>
                <div class="col py-4 px-lg-4 border bg-primary text-center">Message</div>
            </div>
        </div>
    </div>
    @foreach($messages as $message)
    <div class="container mt-2">
        <div class="row">
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$message->name}}</div>
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$message->email}}</div>
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$message->message}}</div>
            <form method="POST" action="{{route('messages.destroy', ['message' => $message->id])}}">
                @method('DELETE')
                @csrf
                <div class="row ml-2" style="height:8px; margin-top:15px; width:8px">
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    @endforeach
</div>
</div>
@endsection