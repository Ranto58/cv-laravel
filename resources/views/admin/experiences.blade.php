@extends('admin.template')

@section('title', 'Mes experiences')

@section('content')
@component('components.experiences', ['experiences' => $experiences, 'mode' => 'edit'])
@endcomponent
@endsection