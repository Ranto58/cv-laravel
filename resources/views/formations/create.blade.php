@extends('admin.template')

@section('title', 'Créer Formation')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('formations.store')}}">
@csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="date">Date</label>
      <input type="text" name ="date" class="form-control" id="date">
    </div>

    <div class="form-group col-md-6">
      <label for="name">Nom de la formation</label>
      <input type="text" name="name" class="form-control" id="name">
    </div>
  </div>
  <div class="form-group">
    <label for="titre">titre</label>
    <input type="titre" name="title" class="form-control" id="titre">
  </div>

  <div class="form-group">
    <label for="description">description</label>
    <textarea type="description" name="description" class="form-control" id="description"></textarea>
  </div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">créer</button>
</form>

@endsection