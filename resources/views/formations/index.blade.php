@extends('layouts.template')
@section('title', 'Mes Formations')
@section('content')
@component('components.formations', ['formations' => $formations, 'mode' => 'normal'])
@endcomponent
@endsection