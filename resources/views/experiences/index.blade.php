@extends('layouts.template')

@section('title', 'Mes experiences')

@section('content')
@component('components.experiences', ['experiences' => $experiences, 'mode' => 'normal'])
@endcomponent
@endsection