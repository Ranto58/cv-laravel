@extends('admin.template')

@section('title', 'Modifier - Expérience')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('experiences.update', ['experience' => $experiences->id])}}">
@method('PUT')
@csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="date">Date</label>
      <input type="text" name ="date" class="form-control" id="date" value="{{$experiences->date}}">
    </div>
    
    <div class="form-group col-md-6">
      <label for="name">Nom de l'entreprise</label>
      <input type="text" name="name" class="form-control" id="name" value="{{$experiences->name}}">
    </div>
  </div>

  <div class="form-group">
    <label for="title">titre</label>
    <input type="title" name="title" class="form-control" id="title" value="{{$experiences->title}}">
  </div>

  <div class="form-group">
    <label for="description">description</label>
    <textarea type="description" name="description" class="form-control" id="description">{{$experiences->description}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">Mettre à jour</button>
</form>
@endsection

