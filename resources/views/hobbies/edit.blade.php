@extends('admin.template')

@section('title', 'Modifier - Hobbie')

@section('content')
<form class="ml-3 mr-3" method="POST" action="{{route('hobbies.update', ['hobby' => $hobbie->id])}}">
@method('PUT')
@csrf
<div class="row">
<div class="form-group">
    <label for="name">name</label>
    <textarea type="name" name="name" class="form-control" id="name">{{$hobbie->name}}</textarea>
  </div>
  <div class="form-group">
    <label for="description">description</label>
    <textarea type="description" name="description" class="form-control" id="description">{{$hobbie->description}}</textarea>
  </div>
</div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">Mettre à jour</button>
</form>
@endsection
