@extends('layouts.template')
@section('title', 'Mes Hobbies')
@section('content')
@component('components.hobbies', ['hobbies' => $hobbies, 'mode' => 'normal'])
@endcomponent
@endsection