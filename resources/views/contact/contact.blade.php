@extends('layouts.template')

@section('content')
<div class="container">
  <form action="/contact" method="POST">
    @csrf
    @method('POST')
    <div class="form-group row mt-5">
      <label for="name" class="col-sm-3 col-form-label">Nom et Prénom</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="name" placeHolder="Nom">
      </div>
    </div>
    <div class="form-group row">
      <label for="email" class="col-sm-3 col-form-label">email</label>
      <div class="col-sm-9">
        <input type="mail" class="form-control" name="email" placeHolder="email">
      </div>
    </div>
    <div class="form-group row">
      <label for="message" class="col-sm-3 col-form-label">Message</label>
      <div class="col-sm-9">
        <textarea type="text" class="form-control" name="message" placeHolder="Message"></textarea>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-9 ">
        <input class="btn btn-primary" type="submit" value="Envoyer">
      </div>
    </div>
  </form>
</div>

@endsection